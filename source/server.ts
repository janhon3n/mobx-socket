import { autorun, extendObservable, isObservable, runInAction } from 'mobx'
import socket from 'socket.io'
import { generateId } from './generate-id'
import { SharedObservable } from './shared-observable'

const io = socket()

export class MobxSocketServer {
   constructor(private port: number) {}

   private sharedStates = new Map<
      string,
      SharedObservable & { __changeId: string | null }
   >()

   private getSharedObservable(
      key: string
   ): SharedObservable & { __changeId: string | null } {
      const observable = this.sharedStates.get(key)
      if (observable == null)
         throw Error(`Shared observable '${key}' does not exist`)
      return observable
   }

   public addSharedObservable(key: string, observable: SharedObservable) {
      if (!isObservable(observable)) throw Error('Object is not an observable')
      if (this.sharedStates.has(key))
         throw Error('Observable object with the same key already exists')

      extendObservable(observable, {
         __changeId: null,
      })

      console.info(`Adding shared state '${key}' to mobx socket server`)
      this.sharedStates.set(
         key,
         observable as SharedObservable & { __changeId: string | null }
      )
   }

   public run() {
      const server = io.listen(this.port)

      server.on('connection', (socket) => {
         console.info(`Client ${socket.id} connected`)
         const subscriptions = new Map<string, () => void>()

         socket.on('subscribe', (key: string) => {
            const observable = this.getSharedObservable(key)

            console.info(`Received new subscription notce for '${key}'`)

            subscriptions.set(
               key,
               autorun(() => {
                  const updatePayload = observable.toTransmit()

                  if (
                     observable.__changeId &&
                     observable.__changeId == pendingChange
                  ) {
                     pendingChange = null
                  } else {
                     console.info(`Sending update for '${key}'`)
                     socket.emit('update-from-server', {
                        key,
                        payload: updatePayload,
                     })
                  }
               })
            )
         })

         let pendingChange: string | null = null

         socket.on('update-from-client', (update) => {
            const observable = this.getSharedObservable(update.key)
            console.info(`Received update from client for '${update.key}'`)
            pendingChange = generateId(5)
            runInAction(() => {
               observable.fromTransmit(update.payload)
               observable.__changeId = pendingChange
            })
         })

         socket.on('unsubscribe', (key: string) => {
            const cancel = subscriptions.get(key)
            if (cancel) {
               cancel()
               subscriptions.delete(key)
            }
         })

         socket.on('disconnect', () => {
            console.info(`Client ${socket.id} disconnected`)
            for (const [key, cancel] of subscriptions) {
               const cancel = subscriptions.get(key)
               if (cancel) {
                  cancel()
                  subscriptions.delete(key)
               }
            }
         })
      })
   }
}
