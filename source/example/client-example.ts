import { runInAction } from 'mobx'
import { MobxSocketClient } from '../client'
import { ExampleState } from './states'

async function main() {
   const client = new MobxSocketClient('ws://localhost:5010')
   const state = new ExampleState()
   client.addSharedObservable('example-state', state)
   client.run()

   setInterval(() => {
      runInAction(() => {
         state.increase()
      })
   }, 3000)
}

main()
