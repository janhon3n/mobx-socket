import { makeAutoObservable } from 'mobx'
import _ from 'lodash'
import { SharedObservable } from '../shared-observable'

export class ExampleState implements SharedObservable {
   constructor() {
      makeAutoObservable(this)
   }

   public number = 1
   public text = 'testi'
   public list = [1, 2, 3, 4, 5]
   public object = {
      a: 1,
      b: '2',
   }

   get sum() {
      return this.number + _.sum(this.list)
   }

   public increase() {
      this.number += 1
   }

   public rotateText() {
      this.text = this.text.substr(1) + this.text[0]
   }

   public doubleList() {
      this.list = this.list.map((i) => i * 2)
   }

   public updateFromJSON(update: string) {
      Object.assign(this, JSON.parse(update))
   }

   public toTransmit() {
      return {
         number: this.number,
         text: this.text,
         list: this.list,
         object: this.object,
      }
   }

   public fromTransmit(update: any) {
      this.number = update.number
      this.text = update.text
      this.list = update.list
      this.object = update.object
   }
}
