import _ from 'lodash'
import { makeAutoObservable, runInAction } from 'mobx'
import { MobxSocketServer } from '../server'
import { ExampleState } from './states'

async function main() {
   const server = new MobxSocketServer(5010)
   const state = new ExampleState()
   server.addSharedObservable('example-state', state)
   server.run()

   setInterval(() => {
      runInAction(() => {
         state.increase()
         state.rotateText()
      })
   }, 2000)
}

main()
