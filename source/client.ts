import { autorun, extendObservable, isObservable, runInAction } from 'mobx'
import io from 'socket.io-client'
import { generateId } from './generate-id'
import { SharedObservable } from './shared-observable'

export class MobxSocketClient {
   constructor(private url: string) {}

   private sharedObservables = new Map<
      string,
      SharedObservable & { __changeId: string | null }
   >()

   public async addSharedObservable(key: string, observable: SharedObservable) {
      if (!isObservable(observable)) throw Error('Object is not an observable')

      extendObservable(observable, {
         __changeId: null,
      })
      console.info(`Adding shared state '${key}' to client`)
      this.sharedObservables.set(
         key,
         observable as SharedObservable & { __changeId: string | null }
      )
   }

   public run() {
      const socket = io(this.url)
      socket.on('connect', () => {
         console.info('Client connected')

         for (const [key, observable] of this.sharedObservables) {
            let pendingChange: string | null = null

            autorun(() => {
               const updatePayload = observable.toTransmit()

               // Dont resend just received update
               if (
                  observable.__changeId &&
                  observable.__changeId === pendingChange
               ) {
                  pendingChange = null
               } else {
                  console.info(`Sending update '${key}' to server`)
                  socket.emit('update-from-client', {
                     key,
                     payload: updatePayload,
                  })
               }
            })

            console.info(`Sending subscriptions notice for ${key}`)
            socket.emit('subscribe', key)

            socket.on('update-from-server', (update: any) => {
               if (update.key !== key) return
               console.info(`Received update from server for '${update.key}'`)
               pendingChange = generateId(5)
               runInAction(() => {
                  observable.fromTransmit(update.payload)
                  observable.__changeId = pendingChange
               })
            })
         }
      })
   }
}
