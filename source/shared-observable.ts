export interface SharedObservable {
   toTransmit: () => any
   fromTransmit: (update: any) => void
}
